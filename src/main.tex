\newcommand{\CLASSINPUTtoptextmargin}{0.745in}
\documentclass[conference]{IEEEtran}
%
\usepackage{nohyperref}
% \usepackage{todonotes}
\usepackage[export]{adjustbox}
\usepackage{tikz}
\usetikzlibrary{arrows, arrows.meta, positioning, backgrounds, calc}
\graphicspath{{./img/}}
\usepackage{dashrule}

\usepackage{amsmath}
\def\Reals{\mathop{\hbox{\mit I\kern-.2em R}}\nolimits}

\usepackage{algorithmic}

\usepackage{array}

\usepackage{url}

\hyphenation{op-tical net-works semi-conduc-tor}

\newcommand{\AnnotatedImage}[2]{%
	\begin{tikzpicture}%
		\node[inner sep=0] (image) at (0,0,0)%
		    {\includegraphics[width=0.48\linewidth]{#1}};%
		\draw%
		    (image.south west)%
		    node[above right=1em, color=white]%
		        {\bf #2};%
	\end{tikzpicture}%
}

\begin{document}

\title{A Marked Point Process Based on a Dictionary for Detecting Small Objects}

\author{\IEEEauthorblockN{Nicolas Cedilnik\\ and Xavier Descombes}
\IEEEauthorblockA{Universit\'e C\^ote d'Azur\\INRIA, I3S\\
2002 route des Lucioles, \\
06903 Sophia Antipolis, France\\
Email: FirstName.Name@inria.fr}
\and
\IEEEauthorblockN{Eric Debreuve}
\IEEEauthorblockA{Universit\'e C\^ote d'Azur\\I3S, INRIA\\
2002 route des Lucioles, \\
06903 Sophia Antipolis, France \\
Email: Eric.Debreuve@i3s.unice.fr}
\and
\IEEEauthorblockN{Fabienne De Graeve\\ and Florence Besse}
\IEEEauthorblockA{Universit\'e C\^ote d'Azur\\ Valrose Biology Institute
Parc Valrose\\
0600 Nice, France\\
Email: FirstName.Name@unice.fr}}

\maketitle

\begin{abstract}
Marked point processes have proven to be very efficient for detecting a collection of objects in an image as they embed geometrical information, constraints on the spatial repartition of objects and image based features.
They are classically defined on sets of parametric shapes such as disks, ellipses or rectangles.
This geometrical representations reach their limit when dealing with small objects of a few pixels.
At this scale the geometrical information embedded in parametric shapes is irrelevant.
In this paper, we consider a marked point process framework for addressing small objects detection by considering a shape space defined by a pre-computed dictionary.
We apply this new scheme for detecting vesicles in cell cytoplasm from confocal microscopy images.
The resulting algorithm, SPADE, is validated on confocal fluorescence microscopy images of \emph{D. melanogaster} cultured cells where cytoplasmic ribonucleoproteic granules have to be detected and characterized.
We demonstrate that our method can be calibrated for high-throughput screening imaging study, generating a $F_1$ detection score of 0.76 after cross-validation using a trained biologist expertise as ground truth.
\end{abstract}

\IEEEpeerreviewmaketitle

\section{Introduction}
\label{sec:intro}

% \subsection{A common task in biological image processing}

The advent of high-troughput microscopy imaging technologies \cite{conrad2010automated} has generated a need for automatic image analysis tools suited for cellular and subcellular object detection and feature extraction.
In such large-scale experiments, a common task is to locate, count and characterize subcellular organelles or macromolecular assemblages such as membranous vesicles, nuclei foci or ribonucleoproteic complexes \cite{smal2010quantitative}.
Automatic detection and identification of such structures is challenging, as some of them have a size close to the microscope resolution.
Furthermore, the signal-to-noise ratio of biological images acquired automatically is heterogeneous and often sub-optimal.

Several methods are available to automatically detect small biological objects \cite{smal2010quantitative}.
The traditional approach is based on thresholding the image intensity, separating the objects from the background.
In order to avoid overdetection, it requires a complex denoising pre-processing step that is particularly unsafe when dealing with objects of small size.
Furthermore, defining a common threshold or a thresholding rule adapted to large sets of images with intra-image and inter-image heterogeneity is usually very difficult.

Another approach is to use a prior on the geometry of the objects of interest.
In this area, wavelet-based methods \cite{olivo2002extraction} are very popular in the field of biological image processing.
However, their output is the objects' locations and does not allow a proper reconstruction of the detected object.

Alternatively, point process-based methods have shown a great potential in the general field of image processing \cite{descombes2013stochastic}.
They also rely on parametric definitions of the objects of interest but their output is more than simple locations: a geometry is provided along with an energy function value for each detection.
Unfortunately, they are not suitable \emph{as is} for the small structures we are discussing here, because these definitions are not adapted for objects that are only a few pixels in size.

In this article we propose a novel method, combining the point process concept of minimizing an energy function with the use of a pre-defined dictionary of shapes, in the spirit of \cite{poulain2015cells} and \cite{perez2012buho}.
We generated such a dictionary automatically based on a set of rules established from our knowledge on the particles of interest in a specific application.
We demonstrated that this method is adapted to the detection of various types of small biological objects, and that it outperforms several popular algorithms when applied to synthetic images with varying blur.

\section{A small particles detector}
\subsection{Marked Point Process}
Marked Point Processes have been successfuly developped for solving objects detection problems in image analysis~\cite{descombes2013stochastic}.
The main idea is to define a probability model on a configuration space defined by sets of objects.
The objects are usually defined in a low dimensionality space $O \subset \Reals^p$.
The configuration space is then defined as $\Omega = \cup_n{\Omega_n}$ where $\Omega_n = (K \times O)^n$ is the set of n objects sets.
$K \ in \Reals^2$ is a compact set embedding the discrete image lattice.
Denote $\pi$ the Poisson process, with intensity $1$ for normalizing, on $K$ and consider a measure, for example the Lebesgue measure $\lambda$, on $O$.
We define a marked point process by a density as follows:
%
\begin{equation}
    \begin{aligned}
\forall \omega  = \{ ((k_i,o_i) i = 1,\dots,n \} \in \Omega, \\
dp(\omega) = h(\omega)d\pi(k)\prod_{i=1}^{n}d\lambda(o_i)
\end{aligned}
\end{equation}
%
where the density $h$ is written as follows: $h(\omega) \exp -U(\omega)$, $U$ being the energy function.
This energy function embeds a prior, penalizing overlaps between neighborhing objects and a data term that fits objects on specific image areas.
Formally, the detection problem is thus solved by optimizing the following energy:
%
\begin{equation}
  U(\omega_1, \omega_2, \dots, \omega_n) = \sum_{i=1}^{n}{u_1(\omega_i)} + \sum_{i,j}{u_2(\omega_i,\omega_j)}
\end{equation}
%
where $n$ is the unknown number of objects in the image, $\omega_i$ is the $i$th unknown object configuration, $u_1(\omega_i)$ is the data term value  and the prior $u_2$ is a pairwise constraint defined as:
%
% \enlargethispage*{2\baselineskip}
%
\begin{equation}
  u_2(\omega_i,\omega_j) =
    \begin{cases}
      0 & \text{ if } \omega_i \cap \omega_j = \emptyset \\
      \infty & \text{ otherwise }
    \end{cases}
\end{equation}

\subsection{A dictionary of shapes}
Classically, the shape space defined by $O$ is a parameter space corresponding to parametric objects such as disk, ellipses or rectangles.
An object is then defind by the value of its marks (radius, orientation, ...).
However, when considering small objects compared to the pixel size the geometrical information is lost and a shape is better described by a set of pixels rather than by a parametric object.
We therefore propose to define $O$ as a dictionary of shapes containing all admissible shapes within a given size range.
The admissibilty of shapes allows to reduce the complexity by imposing some constraints such as the connexity to the considered objects.
In this paper we considered shapes included in a $5 \times 5$ pixels square.
Within this constraint, the dictionary is built iteratively as follows.
We initialize the shape by a segment containing between $1$ and $5$ pixels.
We then consider all the possiblites of adding a line at the top or the bottom with the following criteria:
the new segment is identical as the previous one or can be shifted from one pixel;
the pixel on one end of the segment is removed or pixels at both end are removed;
the segment is removed (see figure~\ref{fig:shapeconstruction}).
Finally we remove shapes of one pixel width and those that are redundant by translation.
A subset of the produced dictionary is shown on figure~\ref{fig:library}.

\begin{figure}
  \centering
  \includegraphics[width=0.98\linewidth]{img/Dico.png}
  \caption{\textbf{Construction of the dictionary} A given shape and the new shapes obatined by adding a segment on the top line.}
  \label{fig:shapeconstruction}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.48\linewidth]{img/potatoids}
  \hfill
  \includegraphics[width=0.48\linewidth]{img/potatoids-rings}
  \caption{\textbf{SPADE default dictionary of shapes.} [Left] Some of the 1383 shapes of the dictionary used in this article. [Right] The corresponding neighboring pixels, used for the score computation.}
  \label{fig:library}
\end{figure}

\subsection{Data fidelity terms}
\label{subsec:data-term}

For a given object configuration $o$, we propose to define data fidelity terms taking the intensities of two sets of pixels as inputs: the set $S(o)$ of the shape pixels, and the set $N(o)$ of the pixels of the neighborhood of the shape (for short, we will use $S$ and $N$ for these sets).
In practice, we chose a neighborhood of 1-pixel thickness (see Fig.~\ref{fig:library}, right).
Note that, following the usual practice, these terms are defined so that they are low when the fidelity to image data is high, and conversely.
Moreover, they should be negative when the pixels of $S$ have in majority a higher intensity than the ones of $N$ so that, by seeking their minimization, we detect objects that are brighter than their background.
If looking for dark objects on a bright background, one must take the opposite of the terms proposed below.

The first two terms we have considered are the difference of means (see Eq.~(\ref{eq:mean})) and the difference medians (see Eq.~(\ref{eq:median})).
The third one is more selective on the contrast between a shape and its neighborhood, but also more sensitive to noise.
It is defined as the difference between the minimum intensity among the pixels of $S$ and the maximum intensity among the ones of $N$ (see Eq.~(\ref{eq:minmax})).
%
% \enlargethispage*{2\baselineskip}
%
\begin{align}
    u_\mathrm{mn}(o) &= \mu_S     - \mu_N     \label{eq:mean}   \\
    u_\mathrm{md}(o) &= \tilde S  - \tilde N  \label{eq:median} \\
    u_\mathrm{mm}(o) &= \min\{S\} - \max\{N\} \label{eq:minmax}
\end{align}
%
where $o$ is an object configuration, and $\mu_X$, $\tilde X$, $\min\{X\}$, and $\max\{X\}$ are respectively the mean, the median, the minimum, and the maximum of the values in set $X$.

We also experimented terms that take the variances of $S$ and $N$ into account.
We used the opposite squared Hellinger distance under the normal distribution assumption (named H-based measure; see Eq.~(\ref{eq:bc}) where $f$ is the identity) but also a modified version (named pseudo-H-based measure; see Eq.~(\ref{eq:bc}) where $f$ is the square root function) that puts less emphasis on the variance, which can be justified by the very small number of pixels considered.
%
\begin{equation}
    u_\mathrm{h(f)}(o) = e^{%
                            - \frac{1}{4}\ln\left(%
                                    \frac{1}{4}\left(\frac{\sigma_{S}^2}{\sigma_{N}^2}
                                + \frac{\sigma_N^2}{\sigma_S^2} + 2\right)\right)
                            - \frac{1}{4}\frac{(\mu _N - \mu _S)^2}{f\left(\sigma_{N}^2 + \sigma_{S}^2\right)}}
                       - 1.
    \label{eq:bc}
\end{equation}
%
See text above for the definition of $f$.

Finally, we experimented the Welch $t$-test (an hypothesis test of identical means of two normally distributed populations with possibly different variances) statistic:
%
\begin{equation}
  u_\mathrm{w}(o) = (\mu_S - \mu_N) / \scriptstyle\sqrt{\frac{\sigma_S^2}{||S||} + \frac{\sigma_N^2}{||N||}}
  \label{eq:t}
\end{equation}
%
where $||X||$ denotes the cardinality of set $X$.

\subsection{Optimization}
Classically marked point processes are optimized using a simulated annealing scheme within an RJMCMC sampler~\cite{green}, a jump diffusion process or a multiple births and deaths algorithm~\cite{zhizhina}.
More recently, graph cut techniques have also been applied in this context~\cite{gamal2010multiple}.
In this paper, we consider a deterministic approach to speed-up the computation time as follows:
%
% \setlength{\leftmargini}{1.5em}
%
\begin{enumerate}
    \setlength{\itemsep}{0.1ex}
    \setlength{\topsep}{0ex}
    %
    \item Consider each of the $5\%$ brightest pixels in the image as potential positions of an object configuration (\emph{i.e.}, as a potential shape center).
    %
    \item For a given potential position, center each shape of the dictionary and compute the corresponding data fidelity term. Retain the one shape that has the minimum term value. If this value is strictly negative (see Section~\ref{subsec:data-term} regarding the sign of data fidelity terms), consider this shape an actual candidate.
    %
    \item List the candidates in order from lowest to highest data fidelity term values.
    %
    \item Parsing this list from first to last, keep the candidates that do not intersect previously listed ones. As a result, we get a set of optimal, non-intersecting objects.
\end{enumerate}
%

Although the convergence of this algorithm is trivial we do not reach the global minimum of the energy.
To provide a simple example consider three objects $\omega_1$, $\omega_2$, $\omega_3$ such that:
$\omega_1 \cap \omega_2 \neq \emptyset$, $\omega_1 \cap \omega_3 \neq \emptyset$, $u_1(\omega_1 < \min (u_1(\omega_2),u_1(\omega_3))$ and $u_1(\omega_1 >  u_1(\omega_2) + u_1(\omega_3)$.
Our algorithm will produce $\{\omega_1\}$ as solution but the optimal one is given by $\{ \omega_2, \omega_3\}$.
However, as we consider very small objects we can expect this situation to be very seldom.

A Python implementation of this method is available as an open source package on the \emph{Python Package Index}: https://pypi.org/pypi/small-particle-detection

% \setlength{\leftmargini}{1.5em}

\section{Experiments on synthetic images}
\label{sec:synth}
\begin{figure}

\smallskip

  \includegraphics[width=0.32\linewidth]{img/simcep-blur-easy}
  \hfill
  \includegraphics[width=0.32\linewidth]{img/simcep-blur-hard}
  \hfill
  \includegraphics[width=0.32\linewidth]{img/simcep-ccd}

\smallskip

  \includegraphics[width=0.32\linewidth]{img/simcep-blur-easy-groundtruth}
  \hfill
  \includegraphics[width=0.32\linewidth]{img/simcep-blur-hard-groundtruth}
  \hfill
  \includegraphics[width=0.32\linewidth]{img/simcep-ccd-groundtruth}
  \caption{\textbf{Synthetic images (top) used for performance evaluation and their corresponding ground truth (bottom).} [Left] ``Easy'' image. [Center] Blurred image. [Right] ``Salt and pepper''-noisy image.}
  \label{fig:synth}
\end{figure}

\subsection{Methodology}
\label{subsec:synth-methodo}

We generated synthetic images using a version of the Matlab\texttrademark{} scripts described in \cite{lehmussola2007computational} modified to better suit our needs.
Namely, we adjusted the scripts to increase the variability in shape of the particles and to remove the nuclei generation that wasn't relevant for our real-case needs.

We generated a set of 1000 8-bit grayscale images of dimensions $250 \times 250$ pixels with varying detection difficulty levels.
For half of the images we fixed the salt and pepper noise (``variance of the coupled charged device'', CCD) while increasing the blurriness and vice-versa for the other half.
The resulting images can be visualized on Fig.~\ref{fig:synth}.

We then compared the detection performance of SPADE (using different data terms) with two methods: a simple thresholding of the image intensity and the wavelet-based spot detector of the Icy software \cite{olivo2002extraction}.
For every method, the parameters were tuned to get the best detection results.

The detection performance was assessed using the $F_1$ score:
\begin{equation}
    \label{eq:f1}
    F_1 = 2\, (\text{precision} \times \text{recall}) / (\text{precision} + \text{recall})
\end{equation}
where recall $= \frac{TP}{TP + FN}$ and precision $= \frac{TP}{TP + FP}$ (TP: true positive, FP: false positive, FN: false negative).

True/false positives and negatives were defined either particle-wise, only evaluating the detector ability to spot a particle while ignoring its ability to also recover the correct shape, or pixel-wise using the Jaccard index between true and detected particle shapes.


\subsection{Results}
\label{subsec:synth-results}

\begin{figure}
    \input{src/isbi-2018-fig-curve}
    \caption{\textbf{SPADE performance.}
        Evaluation of SPADE (with different data fidelity terms)
        against a wavelet-based detector and
        simple intensity thresholding on synthetic images.\newline
        {\small%
        \color{red}\rule[0.2em]{2em}{0.2em}\normalcolor{}~: Intensity thresholding;
        \color{red}\hdashrule[0.2em]{2em}{0.2em}{0.4em 0.15em}\normalcolor{}~: Icy wavelet-based spot detector;
        \color{blue}\rule[0.2em]{2em}{0.2em}\normalcolor{}~: SPADE with {\footnotesize (Sw/ for short)} mean intensity difference;\newline
        \color{blue}\hdashrule[0.2em]{2em}{0.2em}{0.4em 0.15em}\normalcolor{}~: Sw/ median intensity difference;
        \color{blue}\hdashrule[0.2em]{2em}{0.2em}{0.2em 0.15em}\normalcolor{}~: Sw/ ``max/min'' difference;
        \color{green}\rule[0.2em]{2em}{0.2em}\normalcolor{}~: Sw/ Welch t-test;
        \color{green}\hdashrule[0.2em]{2em}{0.2em}{0.4em 0.15em}\normalcolor{}~: Sw/ H-based measure;\newline
        \color{green}\hdashrule[0.2em]{2em}{0.2em}{0.2em 0.15em}\normalcolor{}~: Sw/ pseudo-H-based measure.}
        }
    \label{fig:synth-results}
\end{figure}

In terms of Jaccard index, \emph{i.e.}, the ability to detect the particle with its correct shape, SPADE proved to perform better than the methods it was tested against on all synthetic blurred images with little to no salt and pepper noise, as shown in Fig.~\ref{fig:synth-results}.

Considering the detection rate (\emph{i.e.}, ignoring the particle shape), SPADE was outperformed by methods used for comparison only for very high levels of blur (see Fig.~\ref{fig:synth-results}).

It is worth noting that the best choice of data fidelity term in SPADE may depend on the image degradation level, or otherwise detection difficulty.
Rather than interpreting this as a limitation of SPADE, we see it as an evidence of its versatility with different inputs.

On the opposite, SPADE performed poorly on images with salt and pepper noise (or CCD variance) and was outpaced by the methods used for comparison (see Fig.~\ref{fig:synth-results}, bottom row).
However, on this type of images, the best $F_1$ score was very low no matter the algorithm, questioning the very possibility to exploit data from such images.

\section{Experiments on real data}
\label{subsec:real-case}

\subsection{Methodology}
\label{subsec:calibration}

We then proceeded to evaluate SPADE on a collection of 102 real fluorescence confocal microscopy images, manually annotated by an expert biologist, similar to the ones presented in Fig.~\ref{fig:real} (A).

Since our goal was to focus on the particle detection only, cell cytoplasms were manually segmented on the images.
Furthermore, full automation was crucial for such high-throughput study.
Therefore, we performed the whole detection study with fixed pre-processing and detection parameters.
As a pre-processing step, we tested the normalization of the image intensity variance, mean, median, and the application of a median filter.
All the data fidelity terms presented in Section~\ref{subsec:data-term} were also tested.

We also wanted to test how SPADE performed compared to parametric marked point processes implementations.
To this end, we parameterized ellipses comparable to the objects in our shapes library: 5 pixels maximum for the long axis.
We used the same pre-processing, \emph{neighboring pixels} definition, data fidelity term and maximum energy threshold that were obtained by the cross-validated calibration described in the paragraph above.

\subsection{Results}
\label{real-case-results}
\begin{figure}
  \centering
  \includegraphics[width=0.48\linewidth]{img/orig_}
  \\[0.5em]
  \includegraphics[width=0.48\linewidth]{img/ellipsis_}
  \hfill
  \includegraphics[width=0.48\linewidth]{img/spade_}
  \caption{\textbf{Comparison of detection using a parametric marked point process and SPADE.} [Top] Input image. [Bottom left] Detection with parametrized ellipses. [Bottom right] Detection using SPADE. The parametric marked point process detects too many particles, and their shapes are too constrained by the parametric nature of the process.}
  \label{fig:ellipses}
\end{figure}
\begin{figure}
	\centering
	\AnnotatedImage{img/orig_opera.png}{A}
	\includegraphics[width=0.48\linewidth]{img/spade_opera.png}\\[0.3em]
	\AnnotatedImage{img/orig_incell.png}{B}
	\includegraphics[width=0.48\linewidth]{img/spade_incell.png}\\[0.3em]
	\AnnotatedImage{img/orig_bioch.png}{C}
	\includegraphics[width=0.48\linewidth]{img/spade_bioch.png}\\[0.3em]
	\AnnotatedImage{img/orig_ish.png}{D}
	\includegraphics[width=0.48\linewidth]{img/spade_ish.png}\\[0.3em]
  \caption{\textbf{Spade versatility}.
Examples of real images [Left column] and the corresponding SPADE detections [Right column]. {\small Particles are fusion proteins (Green fluorescent protein-Imp), except (D): in situ hybridization with quasar 570-marked oligonucleotidic probes. Images were acquired using (A) Perkin Elmer OPERA QEHS confocal (63X water objective NA: 1.15); (B) GE Healthcare InCell6000 (60X dry objective NA 0.95); (C) Spinning disk confocal microscope 100X oil objective NA: 1.49; (D) Spinning disk confocal microscope (100X oil objective NA: 1.4).}}
  \label{fig:real}
\end{figure}
%\begin{figure}
%  \begin{tikzpicture}[align=center,node distance=0.1,every node/.style={inner sep=0, outer sep=0}]
%      \node[anchor=south] (orig_opera) at (0,0)
%        {\includegraphics[width=0.24\linewidth]{img/orig_opera.png}};
%        \begin{scope}[x={(orig_opera.south east)},y={(orig_opera.north west)}]
%            \node[white] at (0.1,0.9) {A};
%        \end{scope}
%      \node[right=of orig_opera] (orig_incell)
%        {\includegraphics[width=0.24\linewidth]{img/orig_incell.png}};
%        \begin{scope}[x={(orig_incell.south east)},y={(orig_incell.north west)}]
%            \node[white] at (0.1,0.9) {B};
%        \end{scope}
%      \node[right=of orig_incell] (orig_bioch)
%        {\includegraphics[width=0.24\linewidth]{img/orig_bioch.png}};
%        \begin{scope}[x={(orig_bioch.south east)},y={(orig_bioch.north west)}]
%            \node[white] at (0.1,0.9) {C};
%        \end{scope}
%      \node[right=of orig_bioch] (orig_ish)
%        {\includegraphics[width=0.24\linewidth]{img/orig_ish.png}};
%        \begin{scope}[x={(orig_ish.south east)},y={(orig_ish.north west)}]
%            \node[white] at (0.1,0.9) {D};
%        \end{scope}
%  \end{tikzpicture}
%  \begin{tikzpicture}[align=center,node distance=0.1,every node/.style={inner sep=0, outer sep=0}]
%      \node[anchor=south, below=of orig_opera] (spade_opera)
%        {\includegraphics[width=0.24\linewidth,valign=t]{img/spade_opera.png}};
%      \node[right=of spade_opera] (spade_incell)
%        {\includegraphics[width=0.24\linewidth,valign=t]{img/spade_incell.png}};
%      \node[right=of spade_incell] (spade_bioch)
%        {\includegraphics[width=0.24\linewidth,valign=t]{img/spade_bioch.png}};
%      \node[right=of spade_bioch] (spade_ish)
%        {\includegraphics[width=0.24\linewidth,valign=t]{img/spade_ish.png}};
%  \end{tikzpicture}
%  \caption{\textbf{Spade versatility}.
%Examples of real images (top) and the corresponding SPADE detections (bottom). {\small Particles are fusion proteins (Green fluorescent protein-Imp), except (D): in situ hybridization with quasar 570-marked oligonucleotidic probes. Images were acquired using (A) Perkin Elmer OPERA QEHS confocal (63X water objective NA: 1.15); (B) GE Healthcare InCell6000 (60X dry objective NA 0.95); (C) Spinning disk confocal microscope 100X oil objective NA: 1.49; (D) Spinning disk confocal microscope (100X oil objective NA: 1.4).}}
%  \label{fig:real}
%\end{figure}

Using a leave-one-out cross-validation procedure against our expertise set, we reached the conclusion that the best pre-processing step was the normalization of the mean intensity of each cell and that the optimal data fidelity term for SPADE was the mean difference, in this particular set of images.
With such choices, we reached an $F_1$ detection score of $0.76$.

Using a simple thresholding method, even after mean intensity normalization, this score was only of 0.68.

Using the classic parametric marked point process approach, we could only reach a $F_1$ score of 0.60.
The limited relevance of a geometric definition of particles that have a surface of only a few pixels can account for this important detection difference, see Fig.~\ref{fig:ellipses}.

SPADE is now commonly used by our team to analyze the same or different objects in images taken with other microscopes and different resolutions (see Fig.~\ref{fig:real}).

% \enlargethispage*{2\baselineskip}
%
\section{Conclusion}
\label{sec:conclusion}

SPADE is a promising tool that met our detection requirements.
It has proven to be beneficial against other reference methods for similar tasks in terms of detection quality, although it was computationally more expensive.
It extends the marked point processes framework to objects only a few pixels wide.

We believe that the versatility provided by the choice of different data fidelity terms and dictionaries of shapes is one of its major strengths.

Our future work will focus on integrating it into a fully automated pipeline including cytoplasms segmentations on fluorescence microscopy images.


% conference papers do not normally have an appendix


% use section* for acknowledgment




% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
% \IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
% \IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{src/main}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)
%\begin{thebibliography}{1}

%\bibitem{IEEEhowto:kopka}
%H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
 % 0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.

%\end{thebibliography}




% that's all folks
\end{document}
