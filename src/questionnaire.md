---
geometry: margin=1in
---

# SPADE: a Small Particle Detection method, when a marked point process meets a dictionary of shapes - ISBI 2018 Author Questionnaire

## Regardless of the Application area considered, what is the basic and new technical contribution of this work?

A novel method for small object detection based on a dictionary of shapes and specific data binding functions.

## What is the most similar work that has previously been published by you or any of your coauthors, or that has been accepted/submitted for presentation to other conferences ?

Emmanuelle Poulain, Sylvain Prigent, Emmanuel Soubies, and Xavier Descombes, “Cells detection using segmentation competition,” in Biomedical Imaging (ISBI), 2015 IEEE 12th International Symposium on. IEEE, 2015, pp. 1208–1211.

## How is it different from your new submission? Please include closeness of technical topics as well as the application area.

Here, the objects to segment are much smaller (macromolecular assemblages vs. whole cells). Therefore, we introduce a framework based on dictionaries of shapes and specific data binding functions to match appropriate shapes to image data. A dictionary is generated automatically using rules derived from the expert knowledge on the objects of interest. Hence, the proposed method is adapted to the detection of objects only a few pixel large.

## Provide reference to 3 papers that you are not co-author on that are most closely related to your current submission

- Marcelo Perez-Pepe, Victoria Slomiansky, Mariela Loschi, Luciana Luchelli, Maximiliano Neme, Marı́a Gabriela Thomas, and Graciela Lidia Boccaccio, “BUHO: A MATLAB Script for the Study of Stress Granules and Processing Bodies by High-Throughput Image Analysis,” PloS one, vol. 7, no. 12, pp. E51495, 2012.
- Jean-Christophe Olivo-Marin, “Extraction of spots in biological images using multiscale products,” Pattern recognition, vol. 35, no. 9, pp. 1989–1996, 2002.
- Ihor Smal, Marco Loog, Wiro Niessen, and Erik Meijering, “Quantitative comparison of spot detection methods in fluorescence microscopy,” Medical Imaging, IEEE Transactions on, vol. 29, no. 2, pp. 282–301, 2010.
