FINALNAME=spade-eusipco2018
BUILDDIR=build
LATEX=pdflatex -halt-on-error
BIB=bibtex
SRCDIR=src

MAINFILE=main

N_WORDS := $(shell awk '/\\begin\{abstract\}/{f=1;next} /\\end\{abstract\}/{f=0} f' src/main.tex | wc -w)


TEXFILE=$(SRCDIR)/$(MAINFILE)
all: pdf clean count

pdf:
	-mkdir $(BUILDDIR)
	-mv -n $(BUILDDIR)/* .
	cp src/main.bib .
	-$(BIB) $(MAINFILE).aux
	echo "Rerun to" > $(MAINFILE).log
	while grep "Rerun to\|Rerun LaTeX" $(MAINFILE).log; do \
		$(LATEX) $(TEXFILE).tex; \
		$(BIB) $(MAINFILE).aux; \
	done

clean:
	mkdir -p $(BUILDDIR)
	-mv *.out *.log *.aux *.bbl *.blg *.bib $(BUILDDIR)
	mv $(MAINFILE).pdf $(FINALNAME).pdf
